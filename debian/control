Source: vagrant
Section: admin
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Antonio Terceiro <terceiro@debian.org>,
           Laurent Bigonville <bigon@debian.org>
Build-Depends: bash-completion,
               debhelper-compat (= 13),
               curl,
               gem2deb (>= 1.6),
               rake,
               ruby,
               ruby-bcrypt-pbkdf (>= 1.1),
               ruby-childprocess (>= 4.1.0),
               ruby-ed25519 (>= 1.2.4),
               ruby-erubi,
               ruby-i18n (>= 1.8),
               ruby-listen (>= 3.6),
               ruby-log4r (>= 1.1.9),
               ruby-mime-types (>= 3.3),
               ruby-net-scp (>= 3.0.0),
               ruby-net-sftp (>= 3.0),
               ruby-net-ssh (>= 6.1.0),
               ruby-rspec (>= 3.10),
               ruby-rspec-its,
               ruby-rexml (>= 3.2),
               ruby-vagrant-cloud (>= 3.0.5),
               ruby-webmock,
               ruby-zip (>= 2.0)
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/ruby-team/vagrant.git
Vcs-Browser: https://salsa.debian.org/ruby-team/vagrant
Homepage: https://www.vagrantup.com
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: vagrant
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: libarchive-tools,
         curl,
         openssh-client,
         rsync,
         ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Recommends: vagrant-libvirt
Suggests: virtualbox (>= 4.0)
Breaks: virtualbox (>= ${vagrant:UnsupportedVirtualBox})
Description: Tool for building and distributing virtualized development environments
 This package provides the tools to create and configure lightweight,
 reproducible, and portable virtual environments.
 .
 Vagrant upstream uses Oracle’s VirtualBox by default to create its virtual
 machines. On Debian, Vagrant will use libvirt/KVM by default as VirtualBox is
 not part of Debian main, but will use VirtualBox if it's installed.
